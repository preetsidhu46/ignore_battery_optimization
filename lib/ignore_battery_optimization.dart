import 'dart:async';

import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';

class IgnoreBatteryOptimization {
  static const MethodChannel _channel =
      const MethodChannel('com.hawkeye.battery');
  Function methodCallbackFunction;
  IgnoreBatteryOptimization({@required Function callback}) {
    methodCallbackFunction = callback;
    _channel.setMethodCallHandler((m) => setupMethodHandler(m));
  }
  setupMethodHandler(m) async {
    String channelName = m.method.trim();
    print("channelName =====> $channelName");
  }

  static Future<String> get platformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }

  checkIgnoreBatterOptimizationPermission() async {
    return await _channel.invokeMethod("isBatteryIgnorePermissionAllowed");
  }

  openPermissionDialog() async {
    return await _channel.invokeMethod("batteryIgnoreShowPermission");
  }

  navigateToAutoStartScreen() async {
    return await _channel.invokeMethod("navigateToAutoStart");
  }

  checkPhoneIsRooted() async {
    return await _channel.invokeMethod("isPhoneRooted");
  }

  getDeviceDetails() async {
    return await _channel.invokeMethod("getDeviceDetails");
  }
}
