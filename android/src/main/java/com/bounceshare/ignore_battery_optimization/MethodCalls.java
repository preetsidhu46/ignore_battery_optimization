package com.bounceshare.ignore_battery_optimization;

public class MethodCalls {

    // App white list method call
    final public static String APP_WHITE_LIST_METHOD = "navigateToAutoStart";

    public interface IgnoreBatteryOptimization {
        String SHOW_PERMISSION = "batteryIgnoreShowPermission";
        String IS_PERMISSION_ALLOWED = "isBatteryIgnorePermissionAllowed";
        String AUTO_START_NAVIGATION = "navigateToAutoStart";
        String IS_PHONE_ROOTED = "isPhoneRooted";
        String GET_DEVICE_DETAILS = "getDeviceDetails";
    }
}
