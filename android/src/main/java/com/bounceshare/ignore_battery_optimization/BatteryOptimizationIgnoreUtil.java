package com.bounceshare.ignore_battery_optimization;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Build;
import android.os.PowerManager;
import android.provider.Settings;
import android.util.Log;

import java.util.List;

import static android.content.Context.POWER_SERVICE;

public class BatteryOptimizationIgnoreUtil {

    @SuppressLint("BatteryLife")
    static void showIgnoreBatteryOptimization(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Intent intent = new Intent();
            String packageName = activity.getPackageName();
            Log.e("packageName", "" + packageName);
            if (!isPermissionAllowed(activity)) {
                // intent.setAction(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
                intent.setAction(Settings.ACTION_IGNORE_BATTERY_OPTIMIZATION_SETTINGS);
                // intent.setData(Uri.parse("package:" + packageName));
                activity.startActivityForResult(intent, ActivityRequestCodes.IGNORE_BATTERY_OPTIMIZATION_REQUEST);
            }
        }
    }

    static boolean isPermissionAllowed(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            String packageName = activity.getPackageName();
            Log.e("packageName", "" + packageName);
            PowerManager pm = (PowerManager) activity.getSystemService(POWER_SERVICE);
            return pm.isIgnoringBatteryOptimizations(packageName);
        }
        return true;
    }

    final private static String XIAOMI = "xiaomi";
    final private static String OPPO = "oppo";
    final private static String VIVO = "vivo";
    final private static String LETV = "Letv";
    final private static String HONOR = "Honor";

    public static void appCustomAutoStart(Activity activity) {
        String manufacturer = android.os.Build.MANUFACTURER.toLowerCase();
        Log.e("manufacturer=>", "" + manufacturer);

        try {
            Intent intent = new Intent();
            if (XIAOMI.equalsIgnoreCase(manufacturer)) {
                intent.setComponent(new ComponentName("com.miui.securitycenter",
                        "com.miui.permcenter.autostart.AutoStartManagementActivity"));
            } else if (OPPO.equalsIgnoreCase(manufacturer)) {
                intent.setComponent(new ComponentName("com.coloros.safecenter",
                        "com.coloros.safecenter.permission.startup.StartupAppListActivity"));
            } else if (VIVO.equalsIgnoreCase(manufacturer)) {
                intent.setComponent(new ComponentName("com.vivo.permissionmanager",
                        "com.vivo.permissionmanager.activity.BgStartUpManagerActivity"));
            } else if (LETV.equalsIgnoreCase(manufacturer)) {
                intent.setComponent(new ComponentName("com.letv.android.letvsafe",
                        "com.letv.android.letvsafe.AutobootManageActivity"));
            } else if (HONOR.equalsIgnoreCase(manufacturer)) {
                intent.setComponent(new ComponentName("com.huawei.systemmanager",
                        "com.huawei.systemmanager.optimize.process.ProtectActivity"));
            }
            List<ResolveInfo> list = activity.getPackageManager().queryIntentActivities(intent,
                    PackageManager.MATCH_DEFAULT_ONLY);
            if (list != null && list.size() > 0) {
                activity.startActivityForResult(intent, ActivityRequestCodes.APP_WHITE_LIST_REQUEST);
            } else {
                IgnoreBatteryOptimizationPlugin.handleAppWhiteListRequest(true);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean isParticularManufacturer() {
        String manufacturer = android.os.Build.MANUFACTURER;
        Log.e("manufacturer:", "" + manufacturer);
        boolean status = false;
        switch (manufacturer.toLowerCase()) {
            case XIAOMI:
            case OPPO:
            case VIVO:
            case LETV:
            case HONOR:
                status = true;
                break;
        }

        return status;
    }


}
