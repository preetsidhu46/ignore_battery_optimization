package com.bounceshare.ignore_battery_optimization;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import java.util.HashMap;
import java.util.Map;

import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.plugin.common.PluginRegistry;
import io.flutter.plugin.common.PluginRegistry.Registrar;

/**
 * IgnoreBatteryOptimizationPlugin
 */
public class IgnoreBatteryOptimizationPlugin implements MethodCallHandler, PluginRegistry.ActivityResultListener {

    private static Activity activity;
    private static MethodChannel.Result methodResult;

    /**
     * Plugin registration.
     */
    public static void registerWith(Registrar registrar) {
        final MethodChannel methodChannel = new MethodChannel(registrar.messenger(), MethodChannels.BATTERY_OPTIMIZATION_IGNORE_CHANNEL);
        methodChannel.setMethodCallHandler(new IgnoreBatteryOptimizationPlugin());
        activity = registrar.activity();
        IgnoreBatteryOptimizationPlugin plugin = new IgnoreBatteryOptimizationPlugin();
        registrar.addActivityResultListener(plugin);
    }

    @Override
    public void onMethodCall(MethodCall methodCall, Result result) {
        String callName = methodCall.method.trim();
        switch (callName) {
            case MethodCalls.IgnoreBatteryOptimization.SHOW_PERMISSION:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                    if (BatteryOptimizationIgnoreUtil.isPermissionAllowed(activity)){
                        result.success(true);
                        return;
                    }
                    methodResult = result;
                    BatteryOptimizationIgnoreUtil.showIgnoreBatteryOptimization(activity);
                } else {
                    result.success(true);
                }
                break;
            case MethodCalls.IgnoreBatteryOptimization.IS_PERMISSION_ALLOWED:
                result.success(BatteryOptimizationIgnoreUtil.isPermissionAllowed(activity));
                break;
            case MethodCalls.IgnoreBatteryOptimization.AUTO_START_NAVIGATION:
                if (BatteryOptimizationIgnoreUtil.isParticularManufacturer()){
                    methodResult = result;
                    showAlertDialog();
                } else {
                    result.success(true);
                }
                break;
            case MethodCalls.IgnoreBatteryOptimization.IS_PHONE_ROOTED:
                try {
                    result.success(new RootCheckUtil(activity).isRooted());
                } catch (Exception e) {
                    result.success(false);
                }
                break;
            case MethodCalls.IgnoreBatteryOptimization.GET_DEVICE_DETAILS:
                try {
                    result.success(getDeviceDetails());
                } catch (Exception e) {
                    result.success(new HashMap());
                }
                break;
            default:
                result.notImplemented();
        }
    }

    private Map getDeviceDetails() {
        Map map = new HashMap();
        try {
            map.put("model", Build.MODEL);
            map.put("manufacturer", Build.MANUFACTURER);
            map.put("brand", Build.BRAND);
            map.put("os", Build.VERSION.SDK_INT+"");
            map.put("ctSource","mobile");
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            return map;
        }
    }

    @Override
    public boolean onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.e("IgnP onActivityRe", "" + requestCode);
        switch (requestCode) {
            case ActivityRequestCodes.IGNORE_BATTERY_OPTIMIZATION_REQUEST:
                handleIgnoreBatteryOptimizationsRequest(resultCode == Activity.RESULT_OK);
                break;
            case ActivityRequestCodes.APP_WHITE_LIST_REQUEST:
                handleAppWhiteListRequest(resultCode == Activity.RESULT_OK);
                break;
            default:
                return false;
        }

        return true;
    }

    public static void handleIgnoreBatteryOptimizationsRequest(boolean granted) {
        try {
            if (methodResult == null){
                return;
            }
            methodResult.success(granted);
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }

    }

    private static void showAlertDialog() {
        new AlertDialog.Builder(activity, R.style.Dialog_Theme).setTitle(R.string.action_required)
                .setMessage(R.string.white_list_message).setCancelable(false)
                .setPositiveButton(R.string.navigate_to_settings, (dialog, which) -> {
                    BatteryOptimizationIgnoreUtil.appCustomAutoStart(activity);
                })
                // .setNegativeButton(android.R.string.no, (dialog, which) -> {
                // if (methodResult!=null)
                // methodResult.success(false);
                // })
                .setIcon(android.R.drawable.ic_dialog_alert).show();

    }

    public static void handleAppWhiteListRequest(boolean granted) {
        try {
            if (methodResult == null){
                return;
            }
            methodResult.success(true);
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }

    }
}
