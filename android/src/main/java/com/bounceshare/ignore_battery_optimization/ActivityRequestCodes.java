package com.bounceshare.ignore_battery_optimization;

public class ActivityRequestCodes {
    public static final int IGNORE_BATTERY_OPTIMIZATION_REQUEST = 1002;
    public static final int APP_WHITE_LIST_REQUEST = 1003;
    public static final int BLUETOOTH_PERMISSION_REQUEST = 1004;

}
