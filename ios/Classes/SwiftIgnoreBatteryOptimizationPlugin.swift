import Flutter
import UIKit

public class SwiftIgnoreBatteryOptimizationPlugin: NSObject, FlutterPlugin {
  public static func register(with registrar: FlutterPluginRegistrar) {
    let channel = FlutterMethodChannel(name: "ignore_battery_optimization", binaryMessenger: registrar.messenger())
    let instance = SwiftIgnoreBatteryOptimizationPlugin()
    registrar.addMethodCallDelegate(instance, channel: channel)
  }

  public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
    result("iOS " + UIDevice.current.systemVersion)
  }
}
