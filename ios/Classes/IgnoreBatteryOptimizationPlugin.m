#import "IgnoreBatteryOptimizationPlugin.h"
#import <ignore_battery_optimization/ignore_battery_optimization-Swift.h>

@implementation IgnoreBatteryOptimizationPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftIgnoreBatteryOptimizationPlugin registerWithRegistrar:registrar];
}
@end
